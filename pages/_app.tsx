import '@/styles/globals.css'
import 'mdb-react-ui-kit/dist/css/mdb.min.css'
import '@fortawesome/fontawesome-free/css/all.min.css'
import type { AppProps } from 'next/app'
import { AppContextProvider } from '@context/AppContext'

const App = ({ Component, pageProps }: AppProps) => {
  
  return(
    <AppContextProvider>
      <Component {...pageProps} />
    </AppContextProvider>
  )
}

export default App;
