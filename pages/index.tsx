import React, { useContext, useEffect } from 'react'
import HeadedPage from '@/components/layout/HeadedPage'
import PodcastList from '@/components/layout/podcast/PodcastList'

export default function HomePage() {

  return (
    <HeadedPage>
      <PodcastList/>
    </HeadedPage>
  )
}