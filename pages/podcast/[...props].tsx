import { useRouter } from 'next/router'
import Podcast from '@/components/layout/podcast/Podcast';

  const PODCAST_ID = 0;
  const EPISODE = 1;
  const EPISODE_ID = 2;

const PodcastPage = () => {
  const router = useRouter()
  const { props } = router.query

  return !!props &&
    <Podcast id={props[PODCAST_ID]} idEpisode={props[EPISODE_ID]} detailMode = {!!props[EPISODE] && props[EPISODE]==="episode" && !!props[EPISODE_ID]} />
}

export default PodcastPage