# Noel Cerdán López
This is my approach to the font-end technical test.


## Running development mode:

```bash
npm run dev
```


## Running production mode:
### Build the App
```bash
npm run build
```
### Start the App
```bash
npm run start
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
