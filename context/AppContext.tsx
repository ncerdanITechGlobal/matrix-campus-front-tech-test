import React, { createContext, Dispatch, SetStateAction, useState } from "react";

export type AppContextType = {
  loading: boolean;
  setLoading: Dispatch<SetStateAction<boolean>>;
}

const AppContext = createContext<AppContextType | null>(null);

export type AppContextProps = {
  children: React.ReactNode
}

export const AppContextProvider = (props: AppContextProps) => {
  const {children} = props;
  const [loading, setLoading] = useState(true);
    return (
      <AppContext.Provider value={
          {
            loading, 
            setLoading,
          }
      }>
        {children}
      </AppContext.Provider>
    )
}
export default AppContext;