import useSWR from "swr";
import { PodcastData, PodcastInfoProps } from "@typing/podcast-types";
import moment from "moment";
import {parse} from 'rss-to-json';

export const CACHE_REVALIDATE_MILLIS = 86400000;//24H in millis

export type PodcastParsedResponse = {
  feed: {
    entry:{
      "im:name": {
        label: string;
      },
      "im:image": 
        {
          "label": string;
          "attributes": {
            "height": string;
          }
        }[],
        id: {
          "label": string;
          "attributes": {
            "im:id": string;
          }
        },
        "im:artist": {
          "label": string;
        },
    }[]
 }
}

export type PodcastDetailResponseData = {
  resultCount: number,
  results: {
          collectionId: string;
          artistName: string;
          collectionName: string;
          trackName: string;
          trackCensoredName: string;
          feedUrl: string;
          trackCount: number,
          artworkUrl600: string;
      }[]
}

export type PodcastDetailRssFeedResponse = {

    title: string;
    description: string;
    link: string;
    image: string;
    category: string[];
    items: {
      title: string;
      description: string;
      link: string;
      published: number;
      created: number;
      category: string[];
      content: string;
      enclosures:{
          url: string;
          length: string;
          type: string;
      }[],
      content_encoded: string;
      itunes_episode: string;
      itunes_duration: string;
    }[]
}

export async function fetcher<JSON = any>(
  input: RequestInfo,
  init?: RequestInit
): Promise<JSON> {
  const res = await fetch(input, init)
  const json = await res.json();
  return JSON.parse(json.contents);
}

export const usePodcastData = (id: string) => {
  const urlPodcast = `https://itunes.apple.com/lookup?id=${id}`;
  const url = `https://api.allorigins.win/get?url=${encodeURIComponent(urlPodcast)}`;
  const { data, error, isLoading } = useSWR(url, fetcher, {dedupingInterval: CACHE_REVALIDATE_MILLIS})
  if (!!error) console.error(error);
  return {
      data: parsePodcastDetailData(id, data as PodcastDetailResponseData) ,
      isLoading,
      isError: error
  }
}

export const usePodcastListData = () => {
  const urlGeneral = `https://itunes.apple.com/us/rss/toppodcasts/limit=100/genre=1310/json`;
  const url = `https://api.allorigins.win/get?url=${encodeURIComponent(urlGeneral)}`;
  const { data, error, isLoading } = useSWR(url, fetcher, {dedupingInterval : CACHE_REVALIDATE_MILLIS})
  if (!!error) console.error(error);
  return {
      data: parseRequestData(data as PodcastParsedResponse) ,
      isLoading,
      isError: error
  }
}

export const getRssData = async (rssFeed: string | undefined, data: PodcastInfoProps, setStateAction: (value: React.SetStateAction<PodcastInfoProps>)=>void) => {
  if (!!rssFeed ){
    const parsedRespRssFeed: PodcastDetailRssFeedResponse= await parse(rssFeed);
    const result = addExtraInfoToPodcastData(data, parsedRespRssFeed)
    setStateAction(result);
  }
};

export const parseRequestData = (data: PodcastParsedResponse): PodcastData[] => {
  return data && data.feed.entry.map(elem => {
    ;
    return {
      id: elem.id.attributes['im:id'],
      image: elem['im:image'][2].label,
      title: elem['im:name'].label,
      author: elem['im:artist'].label,
    }
  });
}

const parsePodcastDetailData = (id: string, data: PodcastDetailResponseData): PodcastInfoProps => {
  if (!!data && !! data.resultCount) {
    const {collectionId, trackName, artworkUrl600, artistName, feedUrl} = data.results[0];
    return {
      id: collectionId, 
      title: trackName,
      img: artworkUrl600,
      author: artistName,
      desc: '',
      episodes: [],
      rssFeed: feedUrl,
    }
  }else{
    return {
      id, 
      title: '',
      img: '',
      author: '',
      desc: '',
      episodes: [],
    };
  }
}

export function addExtraInfoToPodcastData(data: PodcastInfoProps, parsedRespRssFeed: PodcastDetailRssFeedResponse): PodcastInfoProps {
  const {title, description, items} = parsedRespRssFeed;
  
  return {
    ...data,
    title,
    desc: description,
    episodes: items && items.map((item, index) => (
      {
        title: item.title,
        description: item.description,
        date: moment(item.published).format('YYYY-MM-DD'),
        duration: calcDuration(item.itunes_duration),
        source: !!item.enclosures[0] ? item.enclosures[0].url : undefined,
        mediaType: !!item.enclosures[0] ? item.enclosures[0].type : undefined,
        idPodcast: data.id,
        idEpisode: index,
        
      }
    )),
  }
}

const calcDuration = (rawDuration: string): string => {
  if (!rawDuration) return '00:00';
  if (typeof rawDuration === "string" && rawDuration.includes(":")) return rawDuration

  const parsedSeconds = typeof rawDuration === "number" ? rawDuration : parseInt(rawDuration);

  return moment("2023-01-01").startOf('day')
    .seconds(parsedSeconds)
    .format('H:mm:ss');
}