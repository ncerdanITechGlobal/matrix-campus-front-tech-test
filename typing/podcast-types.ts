export type PodcastData = {
  id: string;
  image: string;
  title: string;
  author: string;
}

export interface PodcastInfoProps {
  id: string;
  img: string;
  title: string;
  author: string;
  desc: string;
  episodes: EpisodeInfo[];
  rssFeed?: string;
}

export type EpisodeInfo = {
  idEpisode?: number;
  idPodcast?: string;
  title?: string;
  description?: string;
  date?: string;
  duration?: string;
  source?: string;
  mediaType?: string;
}
