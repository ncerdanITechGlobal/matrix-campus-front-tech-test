import * as React from 'react';
import { MDBCard, MDBCardBody, MDBCardText, MDBCardTitle } from 'mdb-react-ui-kit';
import ReactHtmlParser from 'react-html-parser';


export interface PodCastEpisodeDetailProps {
  title?: string,
  desc?: string,
  source?: string;
  mediaType?: string;
}

const PodCastEpisodeDetail: React.FC<PodCastEpisodeDetailProps>  = ({title = '0', desc = '', source = '', mediaType = ''}) => {
  return (
    <MDBCard className='episode-card'>
      <MDBCardTitle>
        {title}
      </MDBCardTitle>
      <MDBCardBody>
        <MDBCardText>
          {ReactHtmlParser(desc)}
        </MDBCardText>
        <hr/>
        <audio controls={true}>
          <source src={source} type={mediaType} />
        </audio>
      </MDBCardBody>
    </MDBCard>
  );
}

export default PodCastEpisodeDetail;