import { MDBContainer, MDBRow, MDBCol, MDBBadge, MDBInput } from 'mdb-react-ui-kit';
import React, { Dispatch, SetStateAction, useContext, useEffect, useState } from 'react';
import { usePodcastListData } from '@helper/swrUtils';
import { PodcastData} from '@/typing/podcast-types';
import PodcastCard from '@/components/layout/podcast/PodcastCard';
import AppContext, { AppContextType } from '@context/AppContext';

export interface PodcastListProps {
}

const filterPodcastList = (e: React.FormEvent<HTMLInputElement>,list: PodcastData[], action: Dispatch<SetStateAction<PodcastData[]>>): void => {
  action(list.filter(elem=>elem.author.toLowerCase().includes(e.currentTarget.value.toLowerCase()) || elem.title.toLowerCase().includes(e.currentTarget.value.toLowerCase())));
}

const PodcastList: React.FC<PodcastListProps> = () => {

  const [podcastList, setPodcastList] = useState<PodcastData[]>([]);

  const { data, isError, isLoading } = usePodcastListData();

  const {setLoading} = useContext(AppContext) as AppContextType;

useEffect(()=>{
  setLoading(isLoading);
},[])

useEffect(()=>{
  if (!!data) {
    setPodcastList(data);
  };
  setLoading(isLoading);
},[isLoading])

  return (
    <MDBContainer>
      <MDBRow>
        <MDBCol size={8} className='d-flex align-items-center justify-content-end'>
          <MDBBadge className='ms-2'>{podcastList.length}</MDBBadge>
        </MDBCol>
        <MDBCol size={4}>
          <MDBInput 
            label='Filter podcast ...' 
            id='input-filter' 
            type='text'
            onChange={(e: React.FormEvent<HTMLInputElement>)=>filterPodcastList(e, data, setPodcastList)}
          />
        </MDBCol>
      </MDBRow>
      <MDBRow className="d-flex justify-content-center podcast-list-row">
        <MDBCol className='podcast-list-col' >
          {
            podcastList.map(elem=> {
              return <PodcastCard key={elem.id} {...elem} />
            })
          }
        </MDBCol>
      </MDBRow>
    </MDBContainer>
  );
}

export default PodcastList;