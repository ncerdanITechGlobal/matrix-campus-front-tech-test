import { MDBCard, MDBCardBody, MDBCardText, MDBCardTitle } from 'mdb-react-ui-kit';
import * as React from 'react';

export interface PodCastEpisodesTotalProps {
  number: string
}

const PodCastEpisodesTotal: React.FC<PodCastEpisodesTotalProps>  = ({number = '0'}) => {
  return (
    <MDBCard className='episode-card d-flex justify-content-start'>
      <MDBCardText className='podcast-total-elements'>
        Episodes: {number}
      </MDBCardText>
    </MDBCard>
  );
}

export default PodCastEpisodesTotal;