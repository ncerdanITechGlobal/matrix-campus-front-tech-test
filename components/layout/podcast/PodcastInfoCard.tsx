import React, {FC} from "react";
import Link from "next/link";
import {
  MDBCard,
  MDBCardBody,
  MDBCardText,
  MDBCardTitle,
  MDBCardSubTitle,
  MDBContainer,
  MDBRow,
  MDBCol,
} from 'mdb-react-ui-kit';
import ReactHtmlParser from 'react-html-parser'
import { PodcastInfoProps } from "@typing/podcast-types";

export type PodcastInfoCard = PodcastInfoProps;

const PodcastInfoCard: FC<PodcastInfoCard> = ({id='', title='', author='', img='', desc=''}) => {
  return (
    <MDBCard>
      <MDBCardBody>
        <MDBContainer>
          <MDBRow>
            <MDBCol className="text-center">
              {!!img && <img className="inner-podcast-img" src={img} alt='...'/>}
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol>
              <MDBCardTitle>
                <Link href={`/podcast/${id}`}>
                  {title}
                </Link>
              </MDBCardTitle>
              <MDBCardSubTitle>
                {!!author && <Link href={`/podcast/${id}`}>
                  <i>by: {author}</i>
                </Link>}
              </MDBCardSubTitle>
              <hr />
              <MDBCardText>
                <b>Description:</b>
                <br />
                {ReactHtmlParser(desc)}
              </MDBCardText>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
      </MDBCardBody>
    </MDBCard>
  )
}

export default PodcastInfoCard;