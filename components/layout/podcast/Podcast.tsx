import React, { useEffect, useState } from 'react';
import AppContext, { AppContextType } from '@context/AppContext';
import HeadedPage from '@components/layout/HeadedPage';
import PodCastEpisodeDetail from '@components/layout/podcast/PodcastEpisodeDetail';
import PodCastEpisodesList from '@components/layout/podcast/PodcastEpisodesList';
import PodCastEpisodesTotal from '@components/layout/podcast/PodcastEpisodesTotal';
import PodcastInfoCard from '@components/layout/podcast/PodcastInfoCard';
import { EpisodeInfo, PodcastInfoProps } from '@typing/podcast-types'
import { getRssData, usePodcastData } from '@/helper/swrUtils';
import { MDBCol, MDBContainer, MDBRow } from 'mdb-react-ui-kit';

export interface PodcastProps {
  id: string,
  detailMode: boolean,
  idEpisode: string;
}

const Podcast:  React.FC<PodcastProps> = ({id, detailMode, idEpisode}) => {
  const emptyPodcastData = {
    id: '',
    img: '',
    title: '',
    author: '',
    desc: '',
    episodes: [] as EpisodeInfo[],
  }

  const {setLoading} = React.useContext(AppContext) as AppContextType
  const {isLoading, isError, data} = usePodcastData(id);
  const [podcastData, setPodcastData] = useState<PodcastInfoProps>({} as PodcastInfoProps);
  
  useEffect(()=> {
    if (!!data && !!data.rssFeed){
      const rss = getRssData;
      rss(data.rssFeed, data, setPodcastData);
    }
    if (!data || isError){
      if (isError) console.error(isError)
      setPodcastData(emptyPodcastData);
    }
    setLoading(isLoading);
  },[isLoading])
  
  useEffect(()=>{
    setLoading(isLoading);
  },[podcastData]);

  const praseNumEpisodeId = (raw: string): number => {
    try {
      return parseInt(raw)
    } catch (error) {
      return 0;
    }
  }

  const numericEpisodeId = praseNumEpisodeId(idEpisode);

  return (
    <HeadedPage>
      <MDBContainer>
        <MDBRow >
          <MDBCol size={3}>
            <PodcastInfoCard {...podcastData}  />
          </MDBCol>
          <MDBCol >

            {!detailMode && <MDBContainer>
              <MDBRow>
                <MDBCol className='justify-content-center'>
                {podcastData.episodes && <PodCastEpisodesTotal number={`${podcastData.episodes.length+''}`} />}
                </MDBCol>
              </MDBRow>
              <MDBRow>
                <MDBCol>
                  <PodCastEpisodesList episodes = {podcastData.episodes}/>
                </MDBCol>
              </MDBRow>
            </MDBContainer>
            }
            {
              podcastData.episodes &&  detailMode && !!podcastData.episodes[numericEpisodeId] && <PodCastEpisodeDetail 
                title={podcastData.episodes[numericEpisodeId].title} 
                desc={podcastData.episodes[numericEpisodeId].description} 
                source={podcastData.episodes[numericEpisodeId].source}
                mediaType={podcastData.episodes[numericEpisodeId].mediaType}
              />
            }
          </MDBCol>
        </MDBRow>
      </MDBContainer>
    </HeadedPage>
  );
}

export default Podcast;
