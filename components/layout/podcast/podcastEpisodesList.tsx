import { MDBCard, MDBCardTitle, MDBTable, MDBTableBody, MDBTableHead } from 'mdb-react-ui-kit';
import Link from 'next/link';
import * as React from 'react';
import { EpisodeInfo } from '@typing/podcast-types';

export type PodCastEpisodesListProps = {
  episodes: EpisodeInfo[]
};

const PodCastEpisodesList: React.FC<PodCastEpisodesListProps>  = ({episodes=[]}) => {
  return (
    <MDBCard className='episode-card episode-card-list'>
      <MDBCardTitle>
          <MDBTable striped>
          <MDBTableHead>
            <tr>
              <th scope='col'>Title</th>
              <th scope='col'>Date</th>
              <th scope='col'>Duration</th>
            </tr>
          </MDBTableHead>
          <MDBTableBody>
            {
              episodes.map(elem=> (
                <tr key={elem.title}>
                <td scope='row'>
                  <Link href={`/podcast/${elem.idPodcast}/episode/${elem.idEpisode}`}>
                    {elem.title}
                  </Link>
                </td>
              <td>{elem.date}</td>
              <td>{elem.duration}</td>
            </tr>    
              ))
            }
          </MDBTableBody>
        </MDBTable>
      </MDBCardTitle>
    </MDBCard>
  );
}

export default PodCastEpisodesList;