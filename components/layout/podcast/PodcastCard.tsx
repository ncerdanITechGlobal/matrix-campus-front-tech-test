import React, {FC} from "react";
import {
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBCardTitle,
  MDBCardSubTitle,
  MDBContainer,
  MDBRow,
  MDBCol,
} from 'mdb-react-ui-kit';
import Link from "next/link";
import { PodcastData } from "@typing/podcast-types";

type PodcastCardProps = PodcastData;

const PodcastCard: FC<PodcastCardProps> = ({id, title = 'title', author  = 'Anonymous', image = ''}) => {
  const url = `/podcast/${id}`;
  return (
    <MDBCard className="custom-card">
      <Link href={url} >
        <MDBCardImage overlay className="custom-card-image" src={image} alt='...' position='top' />
      </Link>
      <MDBCardBody>
        <MDBContainer>
          <MDBRow>
            <MDBCol>
              <Link href={url} >
              <MDBCardTitle>{title}</MDBCardTitle>
              <MDBCardSubTitle>Author: {author}</MDBCardSubTitle>
              </Link>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
      </MDBCardBody>
    </MDBCard>
  )
}

export default PodcastCard;