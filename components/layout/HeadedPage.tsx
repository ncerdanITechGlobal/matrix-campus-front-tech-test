import React, { useContext } from 'react';
import { MDBCol, MDBContainer, MDBRow, MDBSpinner } from 'mdb-react-ui-kit';
import Link from 'next/link';
import AppContext, { AppContextType } from '../../context/AppContext';

interface IHeadedPageProps {
  children?: React.ReactNode;
}

const HeadedPage: React.FunctionComponent<IHeadedPageProps> = (props) => 
   {
      const {loading, setLoading} = useContext(AppContext) as AppContextType;

      const {children} = props;
      return <MDBContainer>
        <MDBRow between className='podcaster-head'>
          <MDBCol size='11'>
              <Link href={'/'} onClick={()=>setLoading(true)}><h1>Podcaster</h1></Link>
          </MDBCol>
          <MDBCol size='1' className='d-flex justify-content-end align-items-center'>
            {loading && <MDBSpinner grow color='primary'>
              <span className='visually-hidden'>Loading...</span>
            </MDBSpinner>}
          </MDBCol>
        </MDBRow>
        <MDBRow>
          <MDBCol>
            {children}
          </MDBCol>
        </MDBRow>
      </MDBContainer>
    }
;

export default HeadedPage;
